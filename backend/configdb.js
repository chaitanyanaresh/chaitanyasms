const mc=require("mongodb").MongoClient
const url="mongodb://motupallinaresh:motupallinaresh@cluster0-shard-00-00-lj8jb.mongodb.net:27017,cluster0-shard-00-01-lj8jb.mongodb.net:27017,cluster0-shard-00-02-lj8jb.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority"
var dbo;
function initDb()
{
    mc.connect(url,{useNewUrlParser:true},(err,client)=>{
        if(err)
        {
            console.log("error in connecting to db")
        };
        console.log("database connected");
        dbo=client.db("sampledatabase")
    })
}function getDb()
{
   return dbo
}
module.exports=
{
    getDb,
    initDb
}