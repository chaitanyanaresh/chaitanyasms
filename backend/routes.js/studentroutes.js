const exp=require("express")
var studentroutes=exp.Router()
const initDb=require('../configdb.js').initDb
const getDb=require('../configdb.js').getDb
const bodyparser=require('body-parser')
const authorization=require("../middleware/authorization")
studentroutes.use(bodyparser.json())
initDb()
studentroutes.get('/readatt',authorization,(req,res,next)=>{
    dbo=getDb()
    
        dbo.collection('attendancecollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:dataArray})
            }
        }
        )
});

studentroutes.get('/readfee',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('feescollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
});

studentroutes.get('/readmr',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('markscollection').find().toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             res.json({message:dataArray})
         }
    })
});
studentroutes.get('/readnotify',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('notificationcollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
         
          
        }
    })
});

studentroutes.post('/viewspecificmarks',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('markscollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             console.log(dataArray)
             res.json({message:dataArray})
         }
    })
});
studentroutes.post('/viewspecificatt',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('attendancecollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             console.log(dataArray)
             res.json({message:dataArray})
         }
    })
});

studentroutes.post('/viewspecificfee',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('feescollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             console.log(dataArray)
             res.json({message:dataArray})
         }
    })
});
studentroutes.post('/viewspecificstd',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('addstudentcollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
});
studentroutes.use((err,req,res,next)=>{
    console.log(err)
})

studentroutes.post('/savereq',authorization,(req,res,next)=>{
    dbo=getDb()
    console.log(req.body)
    if(req.body=={})
    {
        res.json({message:"server did not receive data"})
    }
    else
    {
      dbo.collection("stdreqcollection").insertOne(req.body,(err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else
        {
            dbo.collection("stdrescollection").deleteOne({rollnumber:{$eq:req.body.rollnumber}},(err,dataArray)=>{
             if(err)
             {
                 next(err)
             }
             else
             {
                 res.json({message:"successfully inserted"})
             }
            })
        }
      })
    }
})
studentroutes.post('/viewspecificreq',authorization,(req,res,next)=>{
 dbo=getDb()
 dbo.collection('stdrescollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>{
     if(err)
     {
      next(err)
     }
     else
     {
       res.json({message:dataArray})
     }
 })
});
studentroutes.use((err,req,res,next)=>{
    console.log(err)
})

module.exports=studentroutes