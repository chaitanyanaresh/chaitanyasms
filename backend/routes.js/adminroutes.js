const exp=require("express")
var adminroutes=exp.Router()
const initDb=require('../configdb.js').initDb
const getDb=require('../configdb.js').getDb
const bodyparser=require('body-parser')
const bcrypt=require('bcrypt')
const jwt=require('jsonwebtoken')
const secretkey="amma amma"
const authorization=require("../middleware/authorization")
const nodemailer=require("nodemailer")
const accountSid = 'ACba3b3c9eb650d63cdf63296c44120226';
const authToken = 'bfffb58aa0d246a22ecbc5120b052344';
const client = require('twilio')(accountSid, authToken);
adminroutes.use(bodyparser.json())
initDb()
adminroutes.post('/save',authorization,(req,res,next)=>{
 
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'motupallinaresh@gmail.com', // Your email id
            pass: '85007101' // Your password
        },
        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false
        }
    });
        let info= transporter.sendMail({
            from:'"admin"<motupallinaresh@gmail.com>',
            to:req.body.gmail,
            subject:"student credentials",
            text:`rollnumber: ${req.body.rollnumber},password: ${req.body.password}`
        });
 
 
 
    dbo=getDb()
  dbo.collection("addstudentcollection").find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,studentArray)=>{
   if(studentArray=="") 
   {
  //hash the pwd req.body.password
    bcrypt.hash(req.body.password,6,(err,hashedpassword)=>
    {
        if(err)
        {
            next(err)
        }
        else
        {
          //replace plaintext password with hashedpassword.
            req.body.password=hashedpassword
            console.log(req.body)
            dbo.collection("addstudentcollection").insertOne(req.body,(err,success)=>{
                if(err)
                {
                    next(err)
                }
                else{
                    res.json({message:"success"})
                }
            })
        }
    })
   }
   else{
       res.json({message:"user already exist"})
   }  

})
})
adminroutes.post('/savemr',authorization,(req,res,next)=>{
    dbo=getDb()
    console.log(req.body)
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("markscollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/saveatten',authorization,(req,res,next)=>{
    dbo=getDb()
    console.log(req.body)
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("attendancecollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/savefee',authorization,(req,res,next)=>{
    dbo=getDb()
    console.log(req.body)
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("feescollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/savenotify',authorization,(req,res,next)=>{
    dbo=getDb()
    console.log(req.body)
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("notificationcollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});

adminroutes.post('/saveres',authorization,(req,res,next)=>{
    dbo=getDb()
    console.log(req.body)
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdrescollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
              next(err)   
            }
            else
            {
             dbo.collection("stdreqcollection").deleteOne({rollnumber:{$eq:req.body.rollnumber}},(err,dataArray)=>{
              if(err)
              {
                  next(err)
              }
              else
              {
                  res.json({message:"successfully inserted",data:dataArray})
              }
            })
         }
       }
     )
    }
});
adminroutes.get("/readreq",authorization,(req,res,next)=>{
    dbo.collection("stdreqcollection").find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
})

adminroutes.get('/readstd',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('addstudentcollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
});
adminroutes.post('/saveli',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection('addstudentcollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,userArray)=>{
        console.log(userArray);
        if(userArray.length==0)
        {
            res.json({message:"Invalid student"})
        }
      else{
          bcrypt.compare(req.body.password,userArray[0].password,(err,result)=>{
            if(result==true)
            {
                const signedtoken=jwt.sign({rollnumber:userArray[0].rollnumber},secretkey,{expiresIn:5000})
                console.log(signedtoken)
                res.json({message:"studentlogin success",token:signedtoken,data:userArray})
            }
        else
        {
            res.json({message:"Invalid password"})
        }
    })
    }
    })
});
adminroutes.post('/adminlogin',(req,res,next)=>{
    dbo=getDb()
    console.log("req.body is ",req.body)
    dbo.collection("admin").find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,userArray)=>{
        console.log(userArray.length)
        if(err){
            console.log(err)
        }
        if(userArray.length==0)
        {
            res.json({message:"Invalid admin"})
        }
        else if(userArray[0].password!==req.body.password)
        {
            res.json({message:"Invalid admin password"})
        }
        else{
            const signestoken=jwt.sign({rollnumber:userArray[0].rollnumber},secretkey,{expiresIn:"7d"})
            res.json({message:"admin success",token:signestoken})
        }
    })
}) 

//forgot password
adminroutes.post('/forgotpassword',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    dbo.collection("addstudentcollection").find({rollnumber:req.body.rollnumber}).toArray((err,userArray)=>{
        if(err){
            next(err)
        }
        else{
            if(userArray.length===0){
                res.json({message:"user not found"})
            }
            else{

                jwt.sign({rollnumber:userArray[0].rollnumber},secretkey,{expiresIn:3600},(err,token)=>{
                    if(err){
                     next(err);
                    }
                    else{
                        var OTP=Math.floor(Math.random()*99999)+11111;
                        console.log(OTP)
                         client.messages.create({
                            body: OTP,
                            from: '+12054420317', // From a valid Twilio number
                            to: "+91"+userArray[0].phonenumber,  // Text this number
                        })
                        .then((message) => {
                            dbo.collection('OTPCollection').insertOne({
                                OTP:OTP,
                                rollnumber:userArray[0].rollnumber,
                                OTPGeneratedTime:new Date().getTime()+15000
                        },(err,success)=>{
                            if(err){
                                next(err)
                            }
                            else{                                        
                                res.json({"message":"user found",
                                    "token":token,
                                    "OTP":OTP,
                                    "rollnumber":userArray[0].rollnumber
                                })
                            }
                        })
                        });

                    }
                    
                })
            }
        }
    })
})

//verify OTP
adminroutes.post('/verifyotp',(req,res,next)=>{
    console.log(req.body)
    console.log(new Date().getTime())
    var currentTime=new Date().getTime()
    dbo.collection('OTPCollection').find({"OTP":req.body.OTP}).toArray((err,OTPArray)=>{
        if(err){
            next(err)
        }
        else if(OTPArray.length===0){
            res.json({"message":"invalidOTP"})
        }
        else if(OTPArray[0].OTPGeneratedTime < req.body.currentTime){
            res.json({"message":"invalidOTP"})
        }
        else{
            
            dbo.collection('OTPCollection').deleteOne({OTP:req.body.OTP},(err,success)=>{
                if(err){
                    next(err);
                }
                else{
                    console.log(OTPArray)
                    res.json({"message":"verifiedOTP"})
                }
            })
        }
    })
});

//changing password
adminroutes.put('/changepassword',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    bcrypt.hash(req.body.password,6,(err,hashedPassword)=>{
        if (err) {
            next(err)
        } else {
            console.log(hashedPassword)
           dbo.collection("addstudentcollection").updateOne({rollnumber:req.body.rollnumber},{$set:{
                password:hashedPassword
            }},(err,success)=>{
                if(err){
                    next(err)
                }
                else{
                    res.json({"message":"password changed"})
                }
            }) 
        }
    })
    
});


adminroutes.delete('/deletepro/:rollnumber',authorization,(req,res,next)=>{
    console.log(req.params)
dbo=getDb()
dbo.collection("addstudentcollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
       next(err)
    }
    else
    {
        dbo.collection('addstudentcollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                next(err)
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

});
adminroutes.delete('/deletemr/:rollnumber',authorization,(req,res,next)=>{
    console.log(req.params)
dbo=getDb()
dbo.collection("markscollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
        next(err)
    }
    else
    {
        dbo.collection('markscollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                console.log("error")
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

});
adminroutes.delete('/deleteatt/:rollnumber',authorization,(req,res,next)=>{
    console.log(req.params)
dbo=getDb()
dbo.collection("attendancecollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
        next(err)
    }
    else
    {
        dbo.collection('attendancecollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                console.log("error")
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

});
adminroutes.delete('/deletefee/:rollnumber',authorization,(req,res,next)=>{
    console.log(req.params)
dbo=getDb()
dbo.collection("feescollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
        next(err)
    }
    else
    {
        dbo.collection('feescollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                console.log("error")
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

})
adminroutes.put('/update',authorization,(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    dbo.collection('markscollection').update({rollnumber:{$eq:req.body.rollnumber}},{$set
        :{rollnumber:req.body.rollnumber,subject:req.body.subject,marks:req.body.marks,total:req.body.total}},(err,success)=>{
        if(err){
            next(err)
        }
        else{
            res.json({message:"success"})
        }
    })
});
adminroutes.put('/updateatt',authorization,(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    dbo.collection('attendancecollection').update({rollnumber:{$eq:req.body.rollnumber}},{$set:{rollnumber:req.body.rollnumber,monthattendance:req.body.monthattendance,overallattendance:req.body.overallattendance}},(err,success)=>{
        if(err){
            next(err)
        }
        else{
            res.json({message:"success"})
        }
    })
});
adminroutes.put('/updatefee',authorization,(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    dbo.collection('feescollection').update({rollnumber:{$eq:req.body.rollnumber}},
        {$set:{rollnumber:req.body.rollnumber,totalfee:req.body.totalfee,paidfee:req.body.paidfee,balancefee:req.body.balancefee}},(err,success)=>{
        if(err){
            next(err)
        }
        else{
            res.json({message:"success"})
        }
    })
})

adminroutes.use((err,req,res,next)=>{
    console.log(err)
})

module.exports=adminroutes