import { TestBed } from '@angular/core/testing';

import { datatransferService } from './datatransfer.service';

describe('datatransferService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: datatransferService = TestBed.get(datatransferService);
    expect(service).toBeTruthy();
  });
});
