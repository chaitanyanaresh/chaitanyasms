import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { viewClassName } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class datatransferService {
x:any
a:any
x1:any
a1:any
x2:any
x3:any
x4:any
x5:any
x6:any
x7:any
user:any


  constructor(private hc:HttpClient) { }
  //marks data
  sendDataToservice(a)
  {

    this.x=a;
  }
  getdatafromservice()
  {
    return this.x;
  }
  //attendance data
  senddatatoservice1(a1)
  {
    console.log(a1)
    this.x1=a1;
  }
  getdatafromservice1()
  {
    return this.x1;
  }
  //feestatus data
  senddatatoservice3(a2)
  {
    this.x2=a2
  }
  getdatafromservice2()
  {
    return this.x2
  }
  //notifications
  senddatatoservice4(a3)
  {
    this.x3=a3
  }
  getdatafromservice3()
  {
    return this.x3
  }
  //request
  senddatatoservice5(a4)
  {
    this.x4=a4
  }
  getdatafromservice4()
  {
    return this.x4
  }
  //accept
  senddatatoservice6(a5)
  {
    this.x5=a5
  }
  getdatafromservice5()
  {
    return this.x5
  }
  //reject
  senddatatoservice7(a6)
  {
    this.x6=a6
  }
  getdatafromservice6()
  {
    return this.x6
  }
  sendDataToservice8(a7)
  {
    this.x7=a7
  }
  getdatafromservice7()
  {
    return this.x7
  }
  readData():Observable<any[]>
  {
    return this.hc.get<any[]>('admin/readstd')
  }
  readData1():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readatt')
  }
  readData2():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readfee')
  }
  readData3():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readmr')
  }
  readData4():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readnotify')
  }
  readData5():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readreq')
  }
  loggeduser(user)
  {
    this.user=user[0]
  }
  sendloggeduser()
  {
  return this.user
  }
  viewsepcification(user)
  {
  
    return this.hc.post<any[]>('student/viewspecificmarks',user)
  }
  viewsepcification1(user)
  {
   
    return this.hc.post<any[]>('student/viewspecificatt',user)
  }
  viewsepcification2(user)
  {
    console.log(user)
    return this.hc.post<any[]>('student/viewspecificfee',user)
  }
  viewsepcification3(user)
  {
    console.log(user)
    return this.hc.post<any[]>('student/viewspecificstd',user)
  }
  viewsepcification4(user)
  {
    return this.hc.post<any[]>('student/viewspecificreq',user)
  }

}
