import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { FormsModule } from '@angular/forms';
import { AddstudentComponent } from './adminlogin/addstudent/addstudent.component';
import { StudentprofileComponent } from './adminlogin/studentprofile/studentprofile.component';
import { AttendanceComponent } from './adminlogin/attendance/attendance.component';
import { FeestatusComponent } from './adminlogin/feestatus/feestatus.component';
import { MarksComponent } from './adminlogin/marks/marks.component';
import { NotificationComponent } from './adminlogin/notification/notification.component';
import { RequestComponent } from './adminlogin/request/request.component';
import { LogoutComponent } from './adminlogin/logout/logout.component';


@NgModule({
  declarations: [AdminloginComponent, AddstudentComponent, StudentprofileComponent, AttendanceComponent, FeestatusComponent, MarksComponent, NotificationComponent, RequestComponent, LogoutComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule
  ]
})
export class AdminModule { }
