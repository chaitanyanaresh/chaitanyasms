import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  h:any[]=[];
  constructor(private service1:datatransferService,private httpclient:HttpClient) 
  { }

  ngOnInit()
  {
    this.service1.readData1().subscribe(attendance=>
      {
        this.h=attendance['message']
      })
  }
  sendto1(d)
  {
    if(d.rollnumber=="" || d.monthattendance=="" || d.overallattendance=="")
    {
      alert(["enter the data in the fields"])
    }
    else
    {
    this.h.push(d)
    console.log(d)
    this.httpclient.post("/admin/saveatten",d).subscribe((res)=>
    {
    alert(res['message'])
    }
    )
    this.service1.readData1().subscribe(attendance=>
    {
     this.h=attendance['message']
    })
   }
  }
  
  d:boolean=false;
  objectToModify1:object;
  editRecord(obj1)
  {
    this.objectToModify1=obj1;
    console.log(this.objectToModify1)
    this.d=true
  }
  onSubmit1(modifiedobject1)
  {
    this.d=false
    this. httpclient.put('/admin/updateatt',modifiedobject1).subscribe((res)=>{
    alert(res['message'])
    })
  }
  deleteRecord1(rollnumber)
  {
    console.log(rollnumber)
    this.httpclient.delete(`/admin/deleteatt/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.h=res['data']
    })
  }
}