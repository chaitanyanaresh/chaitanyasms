import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-studentprofile',
  templateUrl: './studentprofile.component.html',
  styleUrls: ['./studentprofile.component.css']
})
export class StudentprofileComponent implements OnInit {
data:any[];
  constructor(private ds8:datatransferService,private httpClient:HttpClient) { }

  ngOnInit() {
    this.ds8.readData().subscribe(data=>
    {
      this.data=data['message']
    })
  }
  deleteRecord3(rollnumber)
  {
    console.log(rollnumber)
    this.httpClient.delete(`/admin/deletepro/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.data=res['data']
    })
  }

}
