import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {
  t:any[]=[];
  constructor(private service3:datatransferService,private htTpclient:HttpClient) { }

  ngOnInit() {
    this.service3.readData2().subscribe(fee=>
      {
        this.t=fee['message']
      })
  }
  sendto2(d6)
  {
    if(d6.rollnumber=="" || d6.totalfee=="" || d6.paidfee=="" || d6.balancefee=="")
    {
     alert(["enter the data in the fields"])
    }
    else
   {
    this.htTpclient.post("/admin/savefee",d6).subscribe((res)=>
    {
    alert(res['message'])
    }
    )
    this.service3.readData2().subscribe(fee=>
    {
    this.t=fee['message']
    })
   }
  } 
  c:boolean=false;
  objectToModify2:object;
  editRecord(obj2)
  {
    this.objectToModify2=obj2;
    console.log(this.objectToModify2)
    this.c=true
  }
  onSubmit2(modifiedobject2)
  {
    this.c=false
    this. htTpclient.put('/admin/updatefee',modifiedobject2).subscribe((res)=>{
      alert(res['message'])
    })
  }
  deleteRecord2(rollnumber)
  {
    console.log(rollnumber)
    this.htTpclient.delete(`/admin/deletefee/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.t=res['data']
    })
  }
}
