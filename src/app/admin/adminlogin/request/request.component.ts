import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

loggedUser;
data:any[]=[]
  constructor(private ds5:datatransferService, private http:HttpClient) { }

  ngOnInit() {
  this.http.get('/admin/readreq').subscribe(data=>{
    this.data=data['message']
  })

  }
  
  send2(rollnumber)
  {
   this.loggedUser=this.ds5.sendloggeduser()
   this.http.post('/admin/saveres',({"message":"request is accepted","rollnumber":rollnumber})).subscribe((res)=>{
     alert(res["message"])
   })
   this.http.get('/admin/readreq').subscribe(data=>{
    this.data=data['message']
  })
  }
  send3(rollnumber)
  {
    this.loggedUser=this.ds5.sendloggeduser()
   this.http.post('/admin/saveres',({"message":"request is rejected","rollnumber":rollnumber})).subscribe((res)=>{
     alert(res["message"])
   })
   this.http.get('/admin/readreq').subscribe(data=>{
    this.data=data['message']
  })
  }
 /* send4(y5)
  {
    this.data3=y5
    this.ds5.sendDataToservice8(this.data3)
  }*/
}