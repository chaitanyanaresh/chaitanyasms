import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
noti:any;
    constructor(private service4:datatransferService,private httPclient:HttpClient) { }
  
    ngOnInit() {
    }
    sendto3(v)
    {
      if(v.notification=="")
      {
        alert(["enter the data in the fields"])
      }
      else
    {
      this.noti=v;
      console.log(v)
      this.httPclient.post("/admin/savenotify",v).subscribe((res)=>
      {
        alert(res['message'])
      }
      )
    }
  }
}