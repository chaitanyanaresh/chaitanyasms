import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {
marks:any[]=[];
constructor(private service:datatransferService,private httpclient:HttpClient) { }
  
ngOnInit() {
  this.service.readData3().subscribe(marks=>
  {
   this.marks=marks['message']
  })
  }
 
  sendData(x)
   {
     if(x.rollnumber==""||x.subject==""||x.marks==""||x.totalmarks=="")
     {
       alert("enter the valid data")
     }
     else{
    this.httpclient.post("/admin/savemr",x).subscribe((res)=>
  {
    alert(res['message'])
  }
  )
   this.service.readData3().subscribe(marks=>
    {
      this.marks=marks['message']
    })
    }}
  deleteRecord(rollnumber)
   {
    console.log(rollnumber)
    this.httpclient.delete(`/admin/deletemr/${rollnumber}`).subscribe((res)=>{
    alert(res['message'])
    this.marks=res['data']
    })
   }
    
    b:boolean=false;
    objectToModify:object;
  editRecord(obj1)
  {
   this.objectToModify=obj1;
   console.log(this.objectToModify)
   this.b=true
  }

  onSubmit(modifiedobject)
  {
   this.b=false
   this.httpclient.put('/admin/update',modifiedobject).subscribe((res)=>{
   alert(res['message'])
  })
  }
}
  