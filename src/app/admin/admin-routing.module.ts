import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AddstudentComponent } from './adminlogin/addstudent/addstudent.component';
import { StudentprofileComponent } from './adminlogin/studentprofile/studentprofile.component';
import { RequestComponent } from './adminlogin/request/request.component';
import { MarksComponent } from './adminlogin/marks/marks.component';
import { FeestatusComponent } from './adminlogin/feestatus/feestatus.component';
import { LogoutComponent } from './adminlogin/logout/logout.component';
import { NotificationComponent } from './adminlogin/notification/notification.component';
import { AttendanceComponent } from './adminlogin/attendance/attendance.component';

const routes: Routes = [
  {
    path:'',
    component:AdminloginComponent,
 children:[   
  {
    path:'addstudent',
    component:AddstudentComponent
  },
  {
    path:'studentprofile',
    component:StudentprofileComponent
  }, 
  {
      path:'marks',
      component:MarksComponent
    },
    {
      path:'attendance',
      component:AttendanceComponent
    },
    {
      path:'feestatus',
      component:FeestatusComponent
    },
    {
      path:'notification',
      component:NotificationComponent
    },
    {
      path:'logout',
      component:LogoutComponent
    },
    {
      path:'request',
      component:RequestComponent
    },]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
