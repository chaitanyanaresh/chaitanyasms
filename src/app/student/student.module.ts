import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { StudentloginComponent } from './studentlogin/studentlogin.component';
import { SprofileComponent } from './studentlogin/sprofile/sprofile.component';
import { SmarksComponent } from './studentlogin/smarks/smarks.component';
import { SattendanceComponent } from './studentlogin/sattendance/sattendance.component';
import { SnotificationComponent } from './studentlogin/snotification/snotification.component';
import { SfeeComponent } from './studentlogin/sfee/sfee.component';
import { SlogoutComponent } from './studentlogin/slogout/slogout.component';
import { StdrequestComponent } from './studentlogin/stdrequest/stdrequest.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [StudentloginComponent, SprofileComponent, SmarksComponent, SattendanceComponent,
     SnotificationComponent,  SlogoutComponent, SfeeComponent, StdrequestComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule
  ]
})
export class StudentModule { }
