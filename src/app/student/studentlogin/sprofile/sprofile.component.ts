import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-sprofile',
  templateUrl: './sprofile.component.html',
  styleUrls: ['./sprofile.component.css']
})
export class SprofileComponent implements OnInit {
data:any[]=[]
user;
  constructor(private ds8:datatransferService) { }
ngOnInit() {
         this.user=this.ds8.sendloggeduser()
         this.ds8.viewsepcification3(this.user).subscribe(data=>
         {
          this.data=data['message']
         })
    }
  }
