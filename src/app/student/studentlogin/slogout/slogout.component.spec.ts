import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlogoutComponent } from './slogout.component';

describe('SlogoutComponent', () => {
  let component: SlogoutComponent;
  let fixture: ComponentFixture<SlogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlogoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
