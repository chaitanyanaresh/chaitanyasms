import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slogout',
  templateUrl: './slogout.component.html',
  styleUrls: ['./slogout.component.css']
})
export class SlogoutComponent implements OnInit {
;
  constructor(private router:Router ) { }

  ngOnInit() {
    localStorage.clear()
    this.router.navigate(['/main/login'])
  }

}
