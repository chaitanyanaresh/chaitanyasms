import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-smarks',
  templateUrl: './smarks.component.html',
  styleUrls: ['./smarks.component.css']
})
export class SmarksComponent implements OnInit {
marks:any[]=[];
user;
 constructor(private std:datatransferService) { }
 
ngOnInit() {
        this.user=this.std.sendloggeduser()
        this.std.viewsepcification(this.user).subscribe(marks=>
        {
          this.marks=marks['message']
        })
    }
   }