import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmarksComponent } from './smarks.component';

describe('SmarksComponent', () => {
  let component: SmarksComponent;
  let fixture: ComponentFixture<SmarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
