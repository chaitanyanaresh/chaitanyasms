import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-stdrequest',
  templateUrl: './stdrequest.component.html',
  styleUrls: ['./stdrequest.component.css']
})
export class StdrequestComponent implements OnInit {
  b:boolean=true;
  accept:any[];
  loggedUser;
  user
  constructor(private service4:datatransferService,private http:HttpClient) { }

  ngOnInit() {
  this.user=this.service4.sendloggeduser()
  this.service4.viewsepcification4(this.user).subscribe(accept=>{
    this.accept=accept['message']
  })
  }
  send1(d6)
  {
    this.loggedUser=this.service4.sendloggeduser();
    d6.rollnumber=this.loggedUser.rollnumber;
    console.log(d6)
    console.log(this.user)
    this.http.post('/student/savereq',d6).subscribe(res=>{
      alert(res['message'])
    })
    this.b=false
  }

}
