import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StdrequestComponent } from './stdrequest.component';

describe('StdrequestComponent', () => {
  let component: StdrequestComponent;
  let fixture: ComponentFixture<StdrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StdrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StdrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
