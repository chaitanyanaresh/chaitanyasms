import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SattendanceComponent } from './sattendance.component';

describe('SattendanceComponent', () => {
  let component: SattendanceComponent;
  let fixture: ComponentFixture<SattendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SattendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SattendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
