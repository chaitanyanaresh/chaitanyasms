import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-sattendance',
  templateUrl: './sattendance.component.html',
  styleUrls: ['./sattendance.component.css']
})
export class SattendanceComponent implements OnInit {
attendance:any[]=[];
user;
constructor(private ds2:datatransferService) { }

ngOnInit() {
            this.user=this.ds2.sendloggeduser()
            this.ds2.viewsepcification1(this.user).subscribe(attendance=>
           {
            this.attendance=attendance['message']
           })
       }
   }
  
