import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-sfee',
  templateUrl: './sfee.component.html',
  styleUrls: ['./sfee.component.css']
})
export class SfeeComponent implements OnInit {
fee:any[]=[];
user;
constructor(private ds3:datatransferService) { }
  
ngOnInit() {
        this.user=this.ds3.sendloggeduser()
        this.ds3.viewsepcification2(this.user).subscribe(fee=>
        {
          this.fee=fee['message']
        })
    }
}
  