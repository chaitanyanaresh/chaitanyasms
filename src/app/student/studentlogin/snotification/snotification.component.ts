import { Component, OnInit } from '@angular/core';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-snotification',
  templateUrl: './snotification.component.html',
  styleUrls: ['./snotification.component.css']
})
export class SnotificationComponent implements OnInit {
notif:any[]=[];
constructor(private ds4:datatransferService ) { }
  
ngOnInit() {    
           this.ds4.readData4().subscribe(notif=>
          {
           this.notif=notif['message']
          })
    }
  }
 
