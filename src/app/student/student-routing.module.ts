import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentloginComponent } from '../student/studentlogin/studentlogin.component';
import { SprofileComponent } from './studentlogin/sprofile/sprofile.component';
import { SmarksComponent } from './studentlogin/smarks/smarks.component';
import { SattendanceComponent } from './studentlogin/sattendance/sattendance.component';
import { SnotificationComponent } from './studentlogin/snotification/snotification.component';
import { SfeeComponent } from './studentlogin/sfee/sfee.component';
import { SlogoutComponent } from './studentlogin/slogout/slogout.component';
import { StdrequestComponent } from './studentlogin/stdrequest/stdrequest.component';

const routes: Routes = [{path:"",component:StudentloginComponent,
                        children:[{path:"sprofile",component:SprofileComponent},
                        {path:"smarks",component:SmarksComponent},
                        {path:"sattendance",component:SattendanceComponent},
                        {path:"snotification",component:SnotificationComponent},
                        {path:"sfee",component:SfeeComponent},
                        {path:"slogout",component:SlogoutComponent},
                        {path:"stdrequest",component:StdrequestComponent}]
                      }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
