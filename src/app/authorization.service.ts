import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements HttpInterceptor{

  constructor() { }
  intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
  {
    //read token from local storage
    const idToken=localStorage.getItem("idToken");
    //if token is not found,it adds it to header of request object
    if(idToken)
    {
      const cloned=req.clone({
        headers:req.headers.set("Authorization","Bearer "+idToken)
      });
      return next.handle(cloned);
    }
    else
    {
      return next.handle(req)
    }
  }
}
