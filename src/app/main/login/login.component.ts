import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginserviceService } from 'src/app/loginservice.service';
import { datatransferService } from 'src/app/datatransfer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router,private http:HttpClient,private loginservice:LoginserviceService,
    private ds10:datatransferService) { }

  ngOnInit() {
  }
  onSubmit(userObject){
  if(userObject.rollnumber==""|| userObject.password=="")
  {
    alert("please enter the details")
  }
  else
  {

    if(userObject.user=="admin")
    {
      this.loginservice.dologinadmin(userObject).subscribe(res=>{
        if(res['message']==="Invalid admin")
        {
          alert("Invalid admin")
        }
        else if(res["message"]==="Invalid admin password")
        {
          alert("wrong password")
        }
        else if(res["message"]==="admin success")
        {
          alert(res["message"]);
          localStorage.setItem('idToken',res['token'])
          this.loginservice.isloggedin=true;
          this.router.navigate(['/admin'])
        }
      })
    }
    else if (userObject.user=="student"){
      console.log(userObject);
      this.loginservice.dologin(userObject).subscribe(res=>{
        if(res["message"]==="Invalid student")
        {
          alert("Invalid student")
        }
        if(res["message"]==="Invalid password")
        {
          alert("Password is not valid")
        }
        if(res["message"]==="studentlogin success")
        {
          alert(res['message'])
          localStorage.setItem('idToken',res['token'])
          this.loginservice.isloggedin=true;
          this.ds10.loggeduser(res['data'])
         this.router.navigate(["/student"]);
        }
      })
   
    }
    
  }
 }
}