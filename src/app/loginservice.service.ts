import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {
isloggedin:boolean=true
  constructor(private hc:HttpClient) { }
  dologin(userObject):Observable<any>
  {
    return this.hc.post<any>('admin/saveli',userObject)
  }
  dologinadmin(userObject):Observable<any>
  {
    return this.hc.post<any>('admin/adminlogin',userObject)
  }
}